# datasizes

A small ansi c program to display the data type sizes of the c compiler being used.
Released under the GPL 3.0 or later license. 

## Getting started

To build it run this command in a terminal in the dowloaded folder.

cc -o ./datasizes ./datasizes.c

Then run the binary or copy it to a directory in your PATH to run it from anywhere.
