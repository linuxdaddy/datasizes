/* datasizes.c --- Prints the sizes for data types for the C compiler
written by Jeff Cataract aka on Linux forums as "linuxdaddy"
last updated on June 24, 2022
Licensed under GPL 3.0 or later
https://www.gnu.org/licenses/gpl-3.0.txt
Copyright 2022 Jeff Cataract
*/

#include <stdio.h>

int main ()
{
		printf("Type int is %d bytes.\n",sizeof(int));
		printf("Type char is %d bytes.\n",sizeof(char));
		printf("Type short is %d bytes.\n",sizeof(short));
		printf("Type unsigned is %d bytes.\n",sizeof(unsigned));
		printf("Type signed is %d bytes.\n",sizeof(signed));
		printf("Type long is %d bytes.\n",sizeof(long));
		printf("Type double is %d bytes.\n",sizeof(double));
		printf("Type float is %d bytes.\n",sizeof(float));
		return 0;		
}
